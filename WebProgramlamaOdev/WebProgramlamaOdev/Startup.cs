﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebProgramlamaOdev.Startup))]
namespace WebProgramlamaOdev
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
