﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebProgramlamaOdev.Models;
using WebProgramlamaOdev.Site;

namespace WebProgramlamaOdev.Controllers
{
    [Authorize]
    public class SatinAlController : Controller
    {
        private hepsiContext ss = new hepsiContext();

        // GET: SatinAl

        // GET: SatinAl/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SatinAl satinAl = ss.SatinAl.Find(id);
            if (satinAl == null)
            {
                return HttpNotFound();
            }
            return View(satinAl);
        }

        // GET: SatinAl/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            return View();
        }

        // POST: SatinAl/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SatinAlID,AdiSoyadi,Email,Adres,KrediKarti")] SatinAl satinAl)
        {
            if (ModelState.IsValid)
            {
                ss.SatinAl.Add(satinAl);
                ss.SaveChanges();
                return RedirectToAction("Details");
            }

            return View(satinAl);
        }

        // GET: SatinAl/Edit/5
        [AllowAnonymous]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SatinAl satinAl = ss.SatinAl.Find(id);
            if (satinAl == null)
            {
                return HttpNotFound();
            }
            return View(satinAl);
        }

        // POST: SatinAl/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Edit([Bind(Include = "SatinAlID,AdiSoyadi,Email,Adres,KrediKarti")] SatinAl satinAl)
        {
            if (ModelState.IsValid)
            {
                ss.Entry(satinAl).State = EntityState.Modified;
                ss.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(satinAl);
        }

        // GET: SatinAl/Delete/5

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SatinAl satinAl = ss.SatinAl.Find(id);
            if (satinAl == null)
            {
                return HttpNotFound();
            }
            return View(satinAl);
        }

        // POST: SatinAl/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SatinAl satinAl = ss.SatinAl.Find(id);
            ss.SatinAl.Remove(satinAl);
            ss.SaveChanges();
            return RedirectToAction("Details");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ss.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
