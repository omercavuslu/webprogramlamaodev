﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using WebProgramlamaOdev.Models;

namespace WebProgramlamaOdev.Site
{
    public class hepsiContext : DbContext
    {
        public hepsiContext()
            : base("hepsiContextEntities2 (WebProgramlamaOdev)")
        {
        }
        public DbSet<Duyurular> Duyurular { get; set; }
        public DbSet<Yorumlar> Yorumlar { get; set; }
        public DbSet<SatinAl> SatinAl { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}