﻿namespace WebProgramlamaOdev.Models
{
    public class RolEkleModel
    {
        public string KullaniciAdi { get; set; }
        public string RolAdi { get; set; }
    }
}